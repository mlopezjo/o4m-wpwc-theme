$(document).ready(function() {
    //console.log( "ready!" );
    $('.filtre a').click(function() {
        var href = $(this).attr('href').substring(1, 3);
        //console.log(href);
        switch (href) {
            case '17':
                //console.log('Pains');
                $('.liste-produits .17').show('slow');
                $('.liste-produits .18').hide('slow');
                $('.liste-produits .19').hide('slow');
                $('.filtre .reset').slideDown('slow');
                break;
            case '18':
                //console.log('Vienoiseries');
                $('.liste-produits .17').hide('slow');
                $('.liste-produits .18').show('slow');
                $('.liste-produits .19').hide('slow');
                $('.filtre .reset').slideDown('slow');
                break;
            case '19':
                //console.log('Confiseries');
                $('.liste-produits .17').hide('slow');
                $('.liste-produits .18').hide('slow');
                $('.liste-produits .19').show('slow');
                $('.filtre .reset').slideDown();
                break;

            default:
                $('.liste-produits *').show('slow');
                $('.filtre .reset').slideUp('slow');
        }
    });
});