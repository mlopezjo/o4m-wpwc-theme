<?php

  $menuLocations = get_nav_menu_locations();
  $menuID = $menuLocations['footerMenu'];
  $primaryNav = wp_get_nav_menu_items($menuID);
  foreach ( $primaryNav as $navItem ) {
    echo '<li><a href="'.$navItem->url.'" title="'.$navItem->title.'">'.$navItem->title.'</a></li>';
  };

?>
