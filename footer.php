</main>
<div class="text-white footer-basic" style="background-color: rgb(33,33,33); margin-top: 50px;">
  <footer>
    <div class="row m-0 p-0">
      <div class="col-md-3 offset-md-1 mb-4 mb-md-0">
        <h4 class="text-center text-md-left">Horaires</h4>
        <ul style="font-size:12px" class="text-center text-md-left">
          <li>Lundi: <span class="text-muted">Fermé</span></li>
          <li>Mardi: <span class="text-muted">6h/12h - 14h/19h</span></li>
          <li>Mercredi: <span class="text-muted">6h/12h - 14h/19h</span></li>
          <li>Jeudi: <span class="text-muted">6h/12h - 14h/19h</span></li>
          <li>Vendredi: <span class="text-muted">6h/12h - 14h/19h</span></li>
          <li>Samedi: <span class="text-muted">6h/12h - 14h/19h</span></li>
          <li>Dimanche: <span class="text-muted">6h/12h</span></li>
        </ul>
      </div>
      <div class="col-md-4 text-center mb-4 mb-md-0">
        <a href="<?= get_site_url();?>"><img style="width:50%;" class="img-fluid" src="<?php echo get_site_url() . '/wp-content/uploads/2020/01/logo.png'; ?>" alt=""></a>
      </div>
      <div class="social col-md-4 d-flex flex-md-column align-items-center justify-content-center">
        <a class="text-white border-white m-2" href="<?php echo get_option('facebook'); ?>"><i class="fa fa-facebook-square"></i></a>
        <a class="text-white border-white m-2" href="<?php echo get_option('instagram'); ?>"><i class="fa fa-instagram"></i></a>
        <a class="text-white border-white m-2" href="<?php echo get_option('tripadvisor'); ?>"><i class="fa fa-tripadvisor"></i></a>
      </div>
    </div>
    
    <p class="copyright"><?php bloginfo('name'); ?> © 2020</p>
    <div class="text-center">    
      <a class="text-white" href="<?= get_site_url() . '/politique-de-confidentialite';?>">Politique de confidentialité</a>
    </div>
  </footer>
  
</div>
<?php wp_footer(); ?>
<script src=<?php echo get_template_directory_uri() . "/assets/js/jquery.min.js"; ?>></script>
<script src=<?php echo get_template_directory_uri() . "/assets/bootstrap/js/bootstrap.min.js"; ?>></script>
</body>

</html>
