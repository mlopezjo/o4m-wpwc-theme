<?php
function add_gcf_interface() {
	add_options_page('Global Custom Fields', 'Informations Générale', '8', 'functions', 'editglobalcustomfields');
}

function editglobalcustomfields() {
	?>
	<div class='wrap'>
	<h2>Informations Générale</h2>
	<form method="post" action="options.php">
	<?php wp_nonce_field('update-options') ?>

	<p><strong>header:</strong><br />
	<input type="file" name="header"/></p>
	<img src="<?php echo get_option('header'); ?>" width="300px" height="auto">
	<p><strong>Facebook:</strong><br />
	<input type="url" name="facebook" size="45" value="<?php echo get_option('facebook'); ?>" /></p>
	<p><strong>Instagram:</strong><br />
	<input type="url" name="instagram" size="45" value="<?php echo get_option('instagram'); ?>" /></p>
	<p><strong>Tripadvisor:</strong><br />
	<input type="url" name="tripadvisor" size="45" value="<?php echo get_option('tripadvisor'); ?>" /></p>

	<p><input type="submit" name="Submit" value="Update Options" /></p>

	<input type="hidden" name="action" value="update" />
	<input type="hidden" name="page_options" value="header,facebook,instagram,tripadvisor" />

	</form>
	</div>
	<?php
}

?>
